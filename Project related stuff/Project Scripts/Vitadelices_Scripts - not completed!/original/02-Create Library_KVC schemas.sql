/*	create Library_KVC schemas
	Script Date: September 12, 2018
	Developed by: 
	Kelly Greyling
	Vahe Hakobyan
	Carlos Coloma
*/

-- switch to the Library_KVC database
use Library_KVC
;
go

-- create schema: Member, Book, and Business
-- create schema schema_name authorization owner_name

create schema Member authorization dbo 
;
go

create schema Book authorization dbo 
;
go

create schema Business authorization dbo 
;
go
