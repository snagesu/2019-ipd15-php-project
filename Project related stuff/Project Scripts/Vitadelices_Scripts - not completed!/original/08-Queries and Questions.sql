/* Queries and Questions in Library_KVC
	Script Date: September 16, 2018
	Developed by: 
	Kelly Greyling
	Vahe Hakobyan
	Carlos Coloma
*/

-- switch to the Library_KVC database
use Library_KVC
;
go

/***********************************************/
/* Query 1. Create a mailing list of Library members that includes 
the members� full names and complete address information.
*/

select  -- MM.MemberID as 'Member ID', 
		concat( MM.FirstName, ' ', (MM.MiddelInitial + ' '), MM.LastName) as 'Member Full Name',
		-- MA.MemberID ,  MJMA.MemberID, 
		concat( coalesce( MA.Street, MJMA.Street), ', ',
			coalesce( MA.City, MJMA.City), ', ',
			coalesce( MA.State, MJMA.State), ', ',
			coalesce( MA.Zip, MJMA.Zip)) as 'Address'
from Member.Member as MM
	left join Member.Adult as MA
		on MM.MemberID = MA.MemberID
	left join 
		(
			select MJ1.MemberID, MA1.Street, MA1.City, MA1.State, MA1.Zip
			from Member.Juvenile as MJ1
				inner join Member.Adult as MA1
					on MJ1.AdultMemberID = MA1.MemberID
		) as MJMA
		on MM.MemberID = MJMA.MemberID
order by coalesce( MA.State, MJMA.State), coalesce( MA.City, MJMA.City), coalesce( MA.Street, MJMA.Street)
;
go


/***********************************************/
/* Query 2. Write and execute a query on the title, item, and copy tables that returns the isbn, 
copy_no, on_loan, title, translation, and cover, and values for rows in the copy table 
with an ISBN of 1 (one), 500 (five hundred), or 1000 (thousand). Order the result by isbn column.
*/

select C.ISBN, C.CopyID, C.OnLoan, T.Title, I.Language, I.Cover
from
	Book.Title as T
join
	Book.Item as I 
on
	T.TitleID = I.TitleID
join
	Book.Copy as C
on
	T.TitleID = C.TitleID
where
	C.ISBN  in (1, 500, 1000)
order by
	C.ISBN, C.CopyID 
;
go

/***********************************************/
/* Query 3- Write and execute a query to retrieve the member�s full name and member_no 
from the member table and the isbn and log_date values from the reservation 
table for members 250, 341, 1675. Order the results by member_no. You should 
show information for these members, even if they have no books or reserve.*/
select 
	concat( MM.FirstName, ' ', (MM.MiddelInitial + ' '), MM.LastName) as 'Member Full Name',
	MM.MemberID as 'Member ID',
	BR.ISBN as 'ISBN - Books on Reservation', BR.LogDate as 'Reservation Date & Time'
from Member.Member as MM
	left outer join Business.Reservation as BR
		on BR.MemberID = MM.MemberID
where MM.MemberID in(250,341,1675)
order by MM.MemberID asc
;
go


/***********************************************/
/* Query 4. Create a view and save it as adultwideView 
that queries the member and adult tables. 
Lists the name & address for all adults.
*/

if OBJECT_ID('Member.adultwideView', 'V') is not null
	drop view Member.adultwideView
;
go

create view Member.adultwideView as
select 
		-- MM.MemberID as 'Member ID', 
		concat( MM.FirstName, ' ', (MM.MiddelInitial + ' '), MM.LastName) as 'Adult Full Name',
		concat( MA.Street, ', ',MA.City, ', ', MA.State, ', ', MA.Zip) as 'Address'
from Member.Member as MM
	inner join Member.Adult as MA
		on MM.MemberID = MA.MemberID
;
go

select *
from Member.adultwideView
;
go


/***********************************************/
/* Query 5. Create a view and save it as ChildwideView that queries 
the member, adult, and juvenile tables. Lists the name & address for the juveniles.
*/

if OBJECT_ID('Member.ChildwideView', 'V') is not null
	drop view Member.ChildwideView
;
go

create view Member.ChildwideView
as
select 
	J.MemberID as 'Juvenile Member ID',
	concat(M.FirstName, ' ', (M.MiddelInitial + ' '), M.LastName) as 'Juvenile Member Name',
	concat(A.Street,', ', A.City, ', ', A.State, ', ', A.Zip) as 'Juvenile Full Address'
from
	Member.Member as M
join
	Member.Juvenile as J
on
	M.MemberID = J.MemberID
join
	Member.Adult as A
on
	J.AdultMemberID = A.MemberID
;
go

select *
from Member.ChildwideView
order by 'Juvenile Member ID'
;
go

/***********************************************/
/* Query 6- Create a view and save it as and CopywideView that queries the copy, 
title and item tables. Lists complete information about each copy.*/
if OBJECT_ID('book.CopywideView','V') is not null
	drop view book.CopywideView
;
go

create view book.CopywideView
as
select C.CopyID as 'Copy ID',c.ISBN,IIF(C.OnLoan = 1, 'Yes', 'No') as 'On Loan',
	IIF(i.Cover = 1, 'Hardcover', 'Softcover') as 'Cover',
	i.Language,IIF(i.Loanable = 1, 'Yes', 'No') as 'Loanable', i.TitleID,
	t.Title,t.Author,t.Synopsis
	
from book.copy as c
	inner join book.item as i
		on c.ISBN=i.ISBN
	inner join book.Title as t
		on t.TitleID=c.TitleID
;
go

select *
from book.CopywideView
;
go

/***********************************************/
/* Query 7. Create a view and save it as LoanableView 
that queries CopywideView (3-table join). 
Lists complete information about each 
copy marked as loanable (loanable = 'Y').
*/
if OBJECT_ID('Book.LoanableView','V') is not null
	drop view Book.LoanableView
;
go


create view Book.LoanableView
as
select *
from Book.CopywideView as C
where C.Loanable = 'Yes'
;
go

select *
from Book.LoanableView
;
go

/***********************************************/
/* Query 8. Create a view and save it as OnshelfView that 
queries CopywideView (3-table join). Lists complete information 
about each copy that is not currently on loan (on_loan ='N').
*/

if OBJECT_ID('Book.OnshelfView', 'V') is not null
	drop view Book.OnshelfView
;
go

create view Book.OnshelfView
as
select *
from Book.CopywideView as C
where C.[On Loan] = 'No'
;
go

select *
from Book.OnshelfView
;
go

/***********************************************/
/* Query 9- Create a view and save it as OnloanView that queries the loan, title, 
and member tables. Lists the member, title, and loan information of a copy 
that is currently on loan.*/
if OBJECT_ID('Business.OnloanView','V') is not null
	drop view Business.OnloanView
;
go

create view Business.OnloanView
as
select
	concat(M.FirstName, ' ', (M.MiddelInitial + ' '), M.LastName) as 'Member Name',
	M.MemberID as 'Member ID',
	t.Title, C.CopyID as 'Copy ID',
	l.OutDate as 'Borrowed Date', l.DueDate as 'Due Date'
from Business.Loan as l
	inner join Member.Member as m
		on l.MemberID=m.MemberID
	inner join Book.Copy as c
		on c.CopyID =l.CopyID	
	inner join Book.Title as t
		on t.TitleID=c.TitleID	
where l.InDate is null
;
go

select *
from Business.OnloanView
;
go

/***********************************************/
/* Query 10. Create a view and save it as OverdueView 
that queries OnloanView (3-table join.) 
Lists the member, title, and loan information 
of a copy on loan that is overdue 
(due_date < current date).
*/
if OBJECT_ID('Business.OverdueView','V') is not null
	drop view Business.OverdueView
;
go

create view Business.OverdueView
as
select
	OLV.[Member Name],
	OLV.[Member ID],
	OLV.Title, OLV.[Copy ID],
	OLV.[Borrowed Date], OLV.[Due Date]
from Business.OnloanView as OLV	
where OLV.[Due Date] < GETDATE()    -- Other date to enter '2018-01-05'
;
go

select *
from Business.OverdueView
;
go

/***********************************************/
/* Question 1. How many loans did the library do last year? */

if OBJECT_ID('Business.TotalLoansLastYearFn', 'Fn') is not null
	drop function Business.TotalLoansLastYearFn
;
go

create function Business.TotalLoansLastYearFn
(
	@Year as int 
)
returns int
as
	begin
		declare @NumberOfLoans as int
		select @NumberOfLoans = count(LoanID)
		from Business.Loan
		where year(OutDate) = @Year
		group by year(OutDate)
		return @NumberOfLoans
	end
;
go

select Business.TotalLoansLastYearFn (2017) as 'Number of loans las year'
;
go

select *
from Business.Loan
where year(OutDate) = 2017
;
go

/***********************************************/
/* Question 2. What percentage of the membership borrowed at least one book?
*/

if OBJECT_ID('Business.membersBorrowedAtLeastOneBookFn', 'Fn') is not null
	drop function Business.membersBorrowedAtLeastOneBookFn
;
go

create function Business.membersBorrowedAtLeastOneBookFn 
(	
	@StartDate as date,
	@EndDate as date
)
returns int
as
	begin
		declare @membersBorrowedAtLeastOneBook int
		select @membersBorrowedAtLeastOneBook = count(distinct(BL.MemberID)) 
		from Business.Loan as BL
		where BL.OutDate between @StartDate and @EndDate
		return  @membersBorrowedAtLeastOneBook
	end
;
go

if OBJECT_ID('Member.quantityOfMembersFn', 'Fn') is not null
	drop function Member.quantityOfMembersFn
;
go

create function Member.quantityOfMembersFn 
(	
)
returns int
as
	begin
		declare @quantityOfMembers int
		select @quantityOfMembers = count(distinct(MM.MemberID))
		from Member.Member as MM
		return  @quantityOfMembers
	end
;
go

if OBJECT_ID('Business.percentageOfMembershipBorrowedAtLeastOneBookFn', 'If') is not null
	drop function Business.percentageOfMembershipBorrowedAtLeastOneBookFn
;
go

create function Business.percentageOfMembershipBorrowedAtLeastOneBookFn 
(	
	@StartDate as date,
	@EndDate as date
)
returns table
as
	return
		select 
			concat('From ', @StartDate, ' to ', @EndDate) as 'Time frame analized',
			(select Business.membersBorrowedAtLeastOneBookFn(@StartDate, @EndDate) )as 'Members that borrowed at least one book',
			(select Member.quantityOfMembersFn()) as 'Quantity of Members',
			concat(convert(decimal(5,2), 
			(100.00 *  Business.membersBorrowedAtLeastOneBookFn(@StartDate, @EndDate)
			/ Member.quantityOfMembersFn() )
			), ' %')
			as 'Percentage of the membership that borrowed at least one book'
;
go

select *
from Business.percentageOfMembershipBorrowedAtLeastOneBookFn('2007-12-20', '2018-12-12')
;
go

/***********************************************/
/*   Question 3. What was the greatest number of books borrowed by any one individual?   */

if OBJECT_ID('Business.MostActiveMember', 'V') is not null
	drop view Business.MostActiveMember
;
go

create view Business.MostActiveMember
as
select 
	M.MemberID as 'Most Active Member ID',
	count(L.LoanID) as 'Number of books borrowed'
from
	Member.Member as M
join
	Business.Loan as L
on 
	L.MemberID = M.MemberID
group by
	M.MemberID
having
    count(L.LoanID) = (
        select 
			max(A.MaxAmount) 
        from (
			select count(L.LoanID) as MaxAmount 
		from 
			Business.Loan as L
		join 
			Member.Member as M
		on 
			L.MemberID = M.MemberID
        group by 
			M.MemberID) as A
	)
;
go

select *
from Business.MostActiveMember
;
go

/***********************************************/
/* Question 4. What percentage of the books was loaned out at least once last year? 
*/

if OBJECT_ID('Business.percentageOfBooksLoanedFn', 'if') is not null
	drop function Business.percentageOfBooksLoanedFn
;
go


create function Business.percentageOfBooksLoanedFn 
(	
	@Year as int
)
returns table
as
		return 
		( 
		select	
				@Year as 'Year analized',	
				count( distinct(l.copyID) ) as 'Quantity of distinct books loaned at least once',
				(select count (c.copyID) from Book.Copy as c )	as 'Total quantity of books',
				concat(convert(decimal(5,2), (count(distinct(l.copyID) )*100.00 / (select count (c.copyID) from Book.Copy as c ))), ' %') as 'percentage'
		from Business.Loan as l
		where year(l.outDate) = @Year
		)
;
go

select *
from Business.percentageOfBooksLoanedFn(2017)
;
go

/***********************************************/
/* Question 5. What percentage of all loans eventually becomes overdue?*/

if OBJECT_ID('Business.quantityOfLoansFn', 'Fn') is not null
	drop function Business.quantityOfLoansFn
;
go

create function Business.quantityOfLoansFn 
(	
	@StartDate as date,
	@EndDate as date
)
returns int
as
	begin
		declare @quantityOfLoans int
		select @quantityOfLoans = count(BL.LoanID)
		from Business.Loan as BL
		where BL.OutDate between @StartDate and @EndDate
		return  @quantityOfLoans
	end
;
go

if OBJECT_ID('Business.quantityOfLoansOverdueFn', 'Fn') is not null
	drop function Business.quantityOfLoansOverdueFn
;
go

create function Business.quantityOfLoansOverdueFn 
(	
	@StartDate as date,
	@EndDate as date
)
returns int
as
	begin
		declare @quantityOfLoansOverdue int
		select @quantityOfLoansOverdue = count(BL.LoanID)
		from Business.Loan as BL
		where (BL.InDate > BL.DueDate or (BL.DueDate < @EndDate And BL.InDate is null) )
			and BL.OutDate between @StartDate and @EndDate
		return  @quantityOfLoansOverdue
	end
;
go

if OBJECT_ID('Business.percentageOfLoansOverdueFn', 'If') is not null
	drop function Business.percentageOfLoansOverdueFn
;
go

create function Business.percentageOfLoansOverdueFn 
(	
	@StartDate as date,
	@EndDate as date
)
returns table
as
	return
		(
		select 
			concat('From ', @StartDate, ' to ', @EndDate) as 'Time frame analized',
			(select Business.quantityOfLoansFn(@StartDate, @EndDate))  as 'Quantity of Loans',
			(select Business.quantityOfLoansOverdueFn(@StartDate, @EndDate)) as 'Quantity of Loans Overdue',
			concat(convert(decimal(5,2), ( 100.00 *  Business.quantityOfLoansOverdueFn(@StartDate, @EndDate)
			/ Business.quantityOfLoansFn(@StartDate, @EndDate) )), ' %') as 'Percentage of Loans Overdue'
		)
;
go

select *
from Business.percentageOfLoansOverdueFn('2007-01-01', '2018-09-20')
;
go

/***********************************************/
/*  Question 6. What is the average length of a loan?  */

if OBJECT_ID('Business.AverageLoanLengthFn', 'Fn') is not null
	drop function Business.AverageLoanLengthFn
;
go

create function Business.AverageLoanLengthFn
(
	@SearchStartDate datetime,
	@SearchEndDate datetime
)
returns int
as
	begin
		declare @LoanDuration as int
		select @LoanDuration = avg( datediff(day, L.OutDate, L.InDate) ) 
		from
			Business.Loan as L
		where
			L.OutDate between @SearchStartDate and @SearchEndDate
			and L.InDate is not null
		return @LoanDuration 
	end
;
go

select [Business].[AverageLoanLengthFn] ('2007-01-01', '2018-09-20') as 'Average days for loans'
;
go

/***********************************************/
/* Question 7: What are the library peak hours for loans? */


if OBJECT_ID('Business.PeakHoursWeekdayPr', 'P') is not null
	drop procedure Business.PeakHoursWeekdayPr
;
go

create procedure Business.PeakHoursWeekdayPr
(
	@StartDate as date,
	@EndDate as date
)
as
	begin
		select top 1 
			concat('From ', @StartDate, ' to ', @EndDate) as 'Time frame analized',
			datepart(hour,BL.OutDate) as 'Weekday Peak Hour',
			(count(BL.copyID)) as 'total books loaned'
		from Business.Loan as BL
		where format(BL.OutDate, 'dddd') in ('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday') and BL.OutDate between @StartDate and @EndDate
		group by datepart(hour,BL.OutDate)
		order by 'total books loaned' desc
	end
;
go


if OBJECT_ID('Business.PeakHoursWeekendPr', 'P') is not null
	drop procedure Business.PeakHoursWeekendPr
;
go

create procedure Business.PeakHoursWeekendPr
(
	@StartDate as date,
	@EndDate as date
)
as
	begin
		select top 1 
			concat('From ', @StartDate, ' to ', @EndDate) as 'Time frame analized',
			datepart(hour,BL.OutDate) as 'Weekend Peak Hour',
			(count(BL.copyID)) as 'total books loaned'
		from Business.Loan as BL
		where format(BL.OutDate, 'dddd') in ('Saturday', 'Sunday') and BL.OutDate between @StartDate and @EndDate
		group by datepart(hour,BL.OutDate)
		order by 'total books loaned' desc
	end
;
go



exec Business.PeakHoursWeekdayPr '01-01-2017', '12-31-2018'
;
go


exec Business.PeakHoursWeekendPr '01-01-2017', '12-31-2018'
;
go

