/* Revise Data in Tables
	DO NOT DELIVER THIS SCRIPT
*/

-- switch to the Library_KVC database
use Library_KVC
;
go


select *
from Member.Member 
;
go

select *
from Member.Adult 
;
go

select *
from Member.Juvenile 
;
go

select *
from Book.Title 
;
go

select *
from Book.Item 
;
go

select *
from Book.Copy 
;
go

select *
from Business.Reservation
;
go

select *
from Business.Loan
;
go

select *
from Business.Loan
;
go

select *
from Business.Message
;
go