/*	create Library_KVC Tables
	Script Date: September 12, 2018
	Developed by: 
	Kelly Greyling
	Vahe Hakobyan
	Carlos Coloma
*/

-- switch to the Library_KVC database
use Library_KVC
;
go

-- 01. create table Member.Member

if OBJECT_ID('Member.Member', 'U') is not null
	drop table Member.Member
;
go

create table Member.Member
(
	MemberID int identity(1,1) not null,
	LastName nvarchar(30) not null,
	FirstName nvarchar(30) not null,
	MiddelInitial nvarchar(2) null,
	Photograph varbinary(max) null,
	constraint PK_Member primary key clustered (MemberID asc)
)
;
go

-- 02. create table Member.Adult

if OBJECT_ID('Member.Adult', 'U') is not null
	drop table Member.Adult
;
go

create table Member.Adult
(
	MemberID int not null, -- foreign key MemberID in Member.Member
	Street nvarchar(60) not null,
	City nvarchar(15) not null,
	State nvarchar(2) not null,
	Zip nvarchar(15) not null,
	PhoneNo nvarchar(20) null,
	ExprDate datetime not null,
	constraint PK_Adult primary key clustered (MemberID asc)
)
;
go

-- 03. create table Member.Juvenile

if OBJECT_ID('Member.Juvenile', 'U') is not null
	drop table Member.Juvenile
;
go

create table Member.Juvenile
(
	MemberID int not null, -- foreign key MemberID in Member.Member
	AdultMemberID int not null, -- foreign key MemberID in Member.Adult
	BirthDate datetime not null,
	constraint PK_Juvenile primary key clustered (MemberID asc)
)
;
go

-- 04. create table Book.Item

if OBJECT_ID('Book.Item', 'U') is not null
	drop table Book.Item
;
go

create table Book.Item
(
	ISBN int not null,
	TitleID int not null, -- foreign key TitleID in Book.Title
	Language nvarchar(20) null,
	Cover bit null,  -- 0 = Softcover, 1 = Hardcover
	Loanable bit null, -- 0 = Not Loanable, 1 = Loanable
	constraint PK_Item primary key clustered (ISBN asc)
)
;
go

-- 05. create table Book.Copy

if OBJECT_ID('Book.Copy', 'U') is not null
	drop table Book.Copy
;
go

create table Book.Copy
(
	CopyID int identity(1,1) not null,
	ISBN int not null, -- foreign key ISBN in Book.Item
	TitleID int not null, -- foreign key TitleID in Book.Title
	OnLoan bit not null,  -- 0 = No, 1 = Yes
	OnHold bit not null,  -- 0 = No, 1 = Yes
	ReservationID int null, -- foreign key ReservationID in Business.Reservation
	StatusGood bit not null,  -- 0 = No, 1 = Yes
	constraint PK_Copy primary key clustered (CopyID asc)
)
;
go

-- 06. create table Book.Title

if OBJECT_ID('Book.Title', 'U') is not null
	drop table Book.Title
;
go

create table Book.Title
(
	TitleID int identity(1,1) not null, 
	Title nvarchar(255) not null,
	Author nvarchar(50) not null,
	Synopsis nvarchar(max) null,
	constraint PK_Title primary key clustered (TitleID asc)
)
;
go

-- 07. create table Business.Reservation

if OBJECT_ID('Business.Reservation', 'U') is not null
	drop table Business.Reservation
;
go

create table Business.Reservation
(
	ReservationID int identity(1,1) not null,
	ISBN int not null, -- foreign key ISBN in Book.Item
	MemberID int not null, -- foreign key MemberID in Member.Member
	LogDate datetime null,
	DateNotified datetime null,
	PickedUp bit null,  -- 0 = No, 1 = Yes
	Remarks nvarchar(max) null,
	constraint PK_Reservation primary key clustered (ReservationID asc)
)
;
go

-- 08. create table Business.Loan

if OBJECT_ID('Business.Loan', 'U') is not null
	drop table Business.Loan
;
go

create table Business.Loan
(
	LoanID int identity(1,1) not null,
	MemberID int not null, -- foreign key MemberID in Member.Member
	CopyID int not null, -- foreign key CopyID in Book.Copy
	OutDate datetime not null,
	DueDate datetime not null,
	InDate datetime null,
	constraint PK_Loan primary key clustered (LoanID asc)
)
;
go

-- 09. create table Business.Fine

if OBJECT_ID('Business.Fine', 'U') is not null
	drop table Business.Fine
;
go

create table Business.Fine
(
	FineID int identity(1,1) not null,
	LoanID int not null, -- foreign key LoanID in Book.Loan
	FineAssessed money not null,
	FinePaid money null,
	FineWaived money null,
	Remarks nvarchar(max) null,
	constraint PK_Fine primary key clustered (FineID asc)
)
;
go

-- 10. create table Business.Message

if OBJECT_ID('Business.Message', 'U') is not null
	drop table Business.Message
;
go

create table Business.Message
(
	MessageID char(4) not null,
	Content text not null,
	constraint PK_Message primary key clustered (MessageID asc)
)
;
go

-- 11. create table Business.MessageLog

if OBJECT_ID('Business.MessageLog', 'U') is not null
	drop table Business.MessageLog
;
go

create table Business.MessageLog
(
	MessageLogID int identity(1,1) not null,
	MessageID char(4) not null,
	LoanID int null, -- foreign key LoanID in Book.Loan
	DateSent datetime not null, -- Date message was sent
	constraint PK_MessageLog primary key clustered (MessageLogID asc)
)
;
go

