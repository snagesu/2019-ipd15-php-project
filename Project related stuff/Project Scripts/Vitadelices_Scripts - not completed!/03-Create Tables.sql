/*	create VitaDelices tables
	Script Date: Jan 15, 2019
	Vahe
	Sugirtha
*/

-- switch to the Library_KVC database
use vitadelices
;


-- 01. create table clients

drop table if exists clients;

create table clients
(	
    clientId int not null AUTO_INCREMENT,
	firstName varchar(30) not null,
	lastName varchar(30) not null,
	companyName varchar(100) null,	
	email varchar(250) not null,
	phone varchar(20) not null,	
	address varchar(100) not null,
	street varchar(60) not null,
	city varchar(15) not null,
	province varchar(2) not null,
	postalCode varchar(7) not null,	
	registrationDate datetime not null,
	password varchar(255) not null,
	constraint pk_clients primary key clustered (clientId asc)
)
;


-- 02. create table orders

drop table if exists orders;

create table orders
(
	orderId int not null AUTO_INCREMENT,	
	orderPlacedDate datetime not null,
	requireddDeliveryDate datetime not null,	
	deliveredDate datetime null,
	status varchar(20) not null, -- to be fixed! It is an Enum - check constraint needed?
	clientId int not null,
	-- adminID int null, 
	shipToName varchar(100) not null,-- can be the combination of fname+lname+business name if any
	shipToAddress varchar(100) not null,
	shipToStreet varchar(60) not null,
	shiptoCity varchar(15) not null,
	shipToProvince varchar(30) not null,
	shipToPostalcode varchar(7) not null,
	comments varchar(2000),	
	constraint pk_orders primary key clustered (orderId asc)
)
;


-- 03. create table products

drop table if exists products;

create table products
(
	productId int not null AUTO_INCREMENT,
	name varchar(50) not null,
	unitPrice decimal(5,2) not null,
	orderId int not null,
	comments varchar(2000),
	primary key (productId)
)
;


-- 04. create table orderdetails
create table orderdetails
(
	orderDetailsId int not null AUTO_INCREMENT,
	orderId int not null, -- FK to orders table
	productId int not null, -- FK to products table
	unitPrice decimal(5,2) not null,-- Price per kilogram
	discount double null,	
	quantity double not null, -- in terms of kilograms
	comments varchar(2000) null,
	constraint pk_orderdetails primary key clustered (orderdetailsId asc)
)
;


-- 05. create table admins

drop table if exists admins;

create table admins
(
	adminId int not null AUTO_INCREMENT,
	firstName varchar(30) not null,
	lastName varchar(30) not null,	
	email varchar(250) not null,
	phone varchar(20) null,		
	password varchar(255) not null,
	primary key (adminId)
)
;


