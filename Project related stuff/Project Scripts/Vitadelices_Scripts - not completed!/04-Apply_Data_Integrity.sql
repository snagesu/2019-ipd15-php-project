/* Apply Data Integrity to Tables in Library_KVC
	Script Date: Jan 15, 2019
	Developed by: 
	Vahe
	Sugirtha
*/

-- Begin

-- switch to the VitaDelices database
use vitadelices
;


-- --- Begin FKs -------

-- 01. create FK in orders table

-- between orders and clients tables
alter table orders
	add constraint fk_orders_users foreign key (clientId)
    references clients (clientId)
;

-- 02. create FK in orderdetails table
-- between orderdetails and orders tables
alter table orderdetails
	add constraint fk_orderdetails_orders foreign key (orderId)
    references orders (orderId)
;


-- between orderdetails and products tables
alter table orderdetails
	add constraint fk_orderdetails_products foreign key (productId)
	references products (productId)
;


-- End ---- FKs -----











/***** Create Constraints in tables *****/

-- 01. make WA the default for state column in adult table
alter table Member.Adult
	add constraint df_State
		default 'WA' for State
;
go

-- 02. create phone number constraint for the adult table
alter table Member.Adult
	add constraint df_PhoneNo
		default Null for PhoneNo
;
go

-- 03. add a default constraint to the DateSent column in the MessageLog table: 
-- DateSent = getdate()
alter table Business.MessageLog
	add constraint df_DateSent
		default getdate() for DateSent
;
go

-- 04. add a constraint to the DueDate column in the loan table: 
-- DueDate >= OutDate
alter table Business.Loan
	add constraint ck_DueDate_Loan
		check (DueDate >= OutDate)
;
go

-- 05. add a constraint to the InDate column in the loan table: 
-- InDate >= OutDate
alter table Business.Loan
	add constraint ck_InDate_Loan
		check (InDate >= OutDate)
;
go

-- 06. add a constraint to the BirthDate column in the Juvenile table: 
-- (BirthDate > DATEADD(year, -18, getdate()))
alter table Member.Juvenile
	add constraint ck_CheckAge_Juvenile
		check (BirthDate > DATEADD(year, -18, getdate()))
;
go

-- 07. make current date the default for LogDate column in Reservation table
alter table Business.Reservation
	add constraint df_LogDate
		default getdate() for LogDate
;
go
