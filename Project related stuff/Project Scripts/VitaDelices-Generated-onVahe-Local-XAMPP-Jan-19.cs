using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace Vitadelices
{
    #region Clients
    public class Clients
    {
        #region Member Variables
        protected int _clientId;
        protected string _firstName;
        protected string _lastName;
        protected string _companyName;
        protected string _email;
        protected string _phone;
        protected string _streetAddress;
        protected string _suiteOrAptNo;
        protected string _city;
        protected unknown _province;
        protected string _postalCode;
        protected DateTime _registrationDate;
        protected string _password;
        #endregion
        #region Constructors
        public Clients() { }
        public Clients(string firstName, string lastName, string companyName, string email, string phone, string streetAddress, string suiteOrAptNo, string city, unknown province, string postalCode, DateTime registrationDate, string password)
        {
            this._firstName=firstName;
            this._lastName=lastName;
            this._companyName=companyName;
            this._email=email;
            this._phone=phone;
            this._streetAddress=streetAddress;
            this._suiteOrAptNo=suiteOrAptNo;
            this._city=city;
            this._province=province;
            this._postalCode=postalCode;
            this._registrationDate=registrationDate;
            this._password=password;
        }
        #endregion
        #region Public Properties
        public virtual int ClientId
        {
            get {return _clientId;}
            set {_clientId=value;}
        }
        public virtual string FirstName
        {
            get {return _firstName;}
            set {_firstName=value;}
        }
        public virtual string LastName
        {
            get {return _lastName;}
            set {_lastName=value;}
        }
        public virtual string CompanyName
        {
            get {return _companyName;}
            set {_companyName=value;}
        }
        public virtual string Email
        {
            get {return _email;}
            set {_email=value;}
        }
        public virtual string Phone
        {
            get {return _phone;}
            set {_phone=value;}
        }
        public virtual string StreetAddress
        {
            get {return _streetAddress;}
            set {_streetAddress=value;}
        }
        public virtual string SuiteOrAptNo
        {
            get {return _suiteOrAptNo;}
            set {_suiteOrAptNo=value;}
        }
        public virtual string City
        {
            get {return _city;}
            set {_city=value;}
        }
        public virtual unknown Province
        {
            get {return _province;}
            set {_province=value;}
        }
        public virtual string PostalCode
        {
            get {return _postalCode;}
            set {_postalCode=value;}
        }
        public virtual DateTime RegistrationDate
        {
            get {return _registrationDate;}
            set {_registrationDate=value;}
        }
        public virtual string Password
        {
            get {return _password;}
            set {_password=value;}
        }
        #endregion
    }
    #endregion
}