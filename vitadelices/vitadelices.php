<?php

session_start();
require_once 'vendor/autoload.php';


if (false) {
	DB::$user = 'vitadelicev';
	DB::$dbName = 'vitadelicev';
	DB::$password = 'hZu7VJ8FxIhJeRuG';
	DB::$port = 3333;
	DB::$host = 'localhost';
	DB::$encoding = 'utf8';
	DB::$error_handler = 'db_error_handler';
} else {
        DB::$user = 'cp4907_vita';
        DB::$dbName = 'cp4907_vitadelicesdb';
        DB::$password = 'hZu7VJ8FxIhJeRuG';
        DB::$encoding = 'utf8';
}
 DB::$error_handler = 'db_error_handler';


//- Log related
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));


 
//- MeekroDB error handler to address database errors 
function db_error_handler($params) {
    global $app, $log;
    $log->error("SQL error: " . $params['error']);
    $log->error("SQL query: " . $params['query']);
    http_response_code(500);
       die;
}


   //DB::$port = 3306;
// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');

//------ Admin and Client Login session accross all pages ------
if (!isset($_SESSION['user'])) {
    $_SESSION['user'] = array();
}

$twig = $app->view()->getEnvironment();
$twig->addGlobal('sessionUser', $_SESSION['user']);

if (!isset($_SESSION['admin'])) {
    $_SESSION['admin'] = array();
}

$twig = $app->view()->getEnvironment();
$twig->addGlobal('sessionAdmin', $_SESSION['admin']);

//------------- End of improved login -------

function renderAllOrders($app) {

        $orderesReceived = DB::query("SELECT * " .
                    " FROM orders as o WHERE o.status = 'received'");
        
        $orderesInProgress = DB::query("SELECT * " .
                    " FROM orders as o WHERE o.status = 'inprogress'");   
         
         $app->render( 'admin_index.html.twig',array(
             'sessionAdmin'=>$_SESSION['admin'],
             'ordReceived' =>$orderesReceived,
             'ordInProgress' => $orderesInProgress
                 )
            );    
}
//- -----loads all orders for an admin user
$app->get('/admin/orders/delivered', function() use ($app) {        
    if(!isset($_SESSION['admin'])){
        return;
    }       
   $orderesDelivered = DB::query("SELECT * " .
                    " FROM orders as o WHERE o.status = 'delivered'");
       
         $app->render( 'orders_delivered.html.twig',array(           
            'ordDelivered' => $orderesDelivered
                 )
            );
});

//- -----loads all orders for an admin user
$app->get('/admin/orders', function() use ($app) {        
    if(!isset($_SESSION['admin'])){
        return;
    }       
    renderAllOrders($app); 
});

//- This route accepts form submissions when changing status of an order - inProgress,delivered
$app->post('/order/:id', function($id) use ($app) {
    //echo "inside /order/$id on post - ";
    
     if (!$_SESSION['admin']) {
        $app->render('access_denied.html.twig');
        return;
    }
    
    $isChangedToInProgress = $app->request()->post('moveToInProgress', 'false');  
    $isChangedToDelivered =  $app->request()->post('moveToDeloivered', 'false');
  
    echo "<br>aaaa $isChangedToInProgress <br>";
    echo "<br>bbb $isChangedToDelivered <br>";
    
      //- If checked, change the status of the order to "inprogress"
    if($isChangedToInProgress=="on"){
        echo "Inside true: isChangedToInProgress value is: $isChangedToInProgress";
        
         DB::update('orders',array(
        'status'=>'inprogress'
      ),"orderId=%i",$id);
    }
    else if ($isChangedToDelivered=="on"){
        DB::update('orders',array(
            'status'=>'delivered',
            
      ),"orderId=%i",$id);        
    }
    else{
        //- Think of all other cases to cover
    }
});

//Invoked from admin profile when clicking an item link
$app->get('/order/:id', function($id) use ($app) {
    //echo "inside /order/$id on get - ";
    
     if (!$_SESSION['admin']) {
        $app->render('access_denied.html.twig');
        return;
    }
    echo "access not denied!";
    $order = DB::queryFirstRow("SELECT *" .
                    " FROM orders as o WHERE o.orderId=%s", $id);

    if($order['status']=="received"){
        $app->render("order_manage_received_to_inprogress.html.twig", array('o' => $order)); 
    }else if($order['status']=="inprogress"){
         $app->render("order_manage_inprogress_to_delivered.html.twig", array('o' => $order));
    }else if($order['status']=="delivered"){
          $app->render("order_delivered.html.twig", array('o' => $order));
    }else{
        
    }
});

//- Begin admin login -----
$app->get('/adminlogin', function() use ($app) {  

    if(!isset($_SESSION['admin'])){
   
    }
    $app->render('admin_login.html.twig'); 
});

//- Begin admin login -----
$app->get('/adminlogin', function() use ($app) {  
    //----  state 1: first show ------
    
    //- Allow login form display only if an admin user hasn't been logged in yet.
    //Otherwis, do not display it. Ex, after logging in, for whatever reason the 
    //admin hits enter on the URL. this is also a "get"
    if(!isset($_SESSION['admin'])){
   
    }
    $app->render('admin_login.html.twig'); 
});

$app->post('/adminlogin', function() use ($app) {
  
    // receiving submission
    $email = $app->request()->post('email');
    $password = $app->request()->post('password');   
    $isLoginSuccessful = false;
    //
    $admin= DB::queryFirstRow("SELECT * FROM admins WHERE email=%s", $email);
     
    if ($admin && ($admin['password'] == $password)) {
        $isLoginSuccessful = true;
    }
    //
    if ($isLoginSuccessful) {
       //---------- state 2: successful submission --------

        unset($admin['password']);
        $_SESSION['admin'] = $admin;
       // echo "Login was successful! - Calling from post(/adminlogin) state 2 - Delete me!";
        
        //- Get all orders with status "received"
        $orderesReceived = DB::query("SELECT * " .
                    " FROM orders as o WHERE o.status = 'received'");

        $orderesInProgress = DB::query("SELECT * " .
                    " FROM orders as o WHERE o.status = 'inprogress'");

         $app->render( 'admin_index.html.twig',array(
             'sessionAdmin'=>$_SESSION['admin'],
             'ordReceived' =>$orderesReceived,
             'ordInProgress' => $orderesInProgress
                 )
            );
    } else {
        //-------  state 3: failed submission
          
        $app->render('admin_login.html.twig', array('error' => true));
          //echo "Login failed! <br>Code Invoked from post(/adminlogin) - State 3";
    }   
  });


$app->get('/admin', function() use ($app) {
   $app->render('admin_index.html.twig');   
   
});

//------ Begin Client login/logout ----------
$app->get('/login', function() use ($app) {
    // state 1: first show
    $app->render('login.html.twig');
});

$app->post('/login', function() use ($app) {
    // receiving submission
    $email = $app->request()->post('email');
    $password = $app->request()->post('password');   
    $isLoginSuccessful = false;
    //
    $user = DB::queryFirstRow("SELECT * FROM clients WHERE email=%s", $email);
     
     //- Ensures this user exists in the DB, and the imput password is OK.
    if ($user && ($user['password'] == $password)) {
        $isLoginSuccessful = true;
    }
    //
    if ($isLoginSuccessful) {
       //---------- state 2: successful submission --------
        unset($user['password']);
        $_SESSION['user'] = $user;
        //echo "Login was successful! - Calling from post(/login) state 2 - Delete me!";
        
        //- Rendering index file, as well as sending sessionUser value to the master table
        $app->render( 'index.html.twig',array('sessionUser'=>$_SESSION['user']) );
        
        //$app->render('login_success.html.twig', array('sessionUser' => $_SESSION['user']));
    } else {
        //-------  state 3: failed submission
          
        $app->render('login.html.twig', array('error' => true));
    }
});

//- Clients and admins logout 
$app->get('/logout', function() use ($app) {

    //echo "Inside /logout...<br>";
    if( $_SESSION['user']){
        //echo "Inside /logout - user";
       $_SESSION['user'] = array();
       $currSession = array('sessionUser' => $_SESSION['user']);
       
    }else if($_SESSION['admin']){
        //echo "Inside /logout - admin";
       $_SESSION['admin'] = array(); 
       $currSession= array('sessionAdmin' => $_SESSION['admin']);
    }

     $app->render('index.html.twig',$currSession);    
    
});//------ End Session login\Logout -----

//------- Beginning of the cart information ------------
//Technically getting the information from the database
$app->get('/', function() use ($app) {
    $productLists = DB::query("SELECT * FROM products");
    $app->render('index.html.twig', array(
        'productLists' => $productLists
    ));
});
//--------------------------------------------------------

$app->get('/', function() use ($app) {
	$orderLists = DB::query(
                "SELECT od.productId, o.quantity, o.requiredDeliveryDate "
                . "FROM orderdetails as od, orders as o "
                . "WHERE od.orderId = o.orderId");
        $app->render('index.html.twig', array(
            'orderLists' => $orderLists		
    ));
});
//--------------------------------------------------------
//- Index/Home page
$app->get('/', function() use ($app) {

   if( !$_SESSION['admin']){
       $app->render('index.html.twig');
   }else{
       $app->render('admin_index.html.twig');
   }
});

//---- Begining - user Registration 
$app->get('/register', function() use ($app){
    $app->render('register.html.twig');
});

$app->post('/register', function() use ($app,$log){
    
    $firstName = $app->request()->post('firstName');  
    $lastName = $app->request()->post('lastName');
    $companyName =$app->request()->post('companyName');
    $streetAddress = $app->request()->post('streetAddress');
    $suiteOrAptNo = $app->request()->post('suiteOrAptNo');
    $city = $app->request()->post('city');
    $province = $app->request()->post('province');
    $postalCode = $app->request()->post('postalCode');
    $phone = $app->request()->post('phone');
    $email1 = $app->request()->post('email1');
    $email2 = $app->request()->post('email2');
    $pass1 = $app->request()->post('pass1');
    $pass2 = $app->request()->post('pass2');
    
    //- Helps accumulating error cases
    $errorList = array();
    
    //- Helps makyng form fields sticky. We render this array back to the form
     $valueList = array();     
     
     if (strlen($firstName) < 2 || strlen($firstName) > 30 || 
             preg_match("/^[a-zA-Z]+$/", $firstName) === 0 ){
        array_push($errorList, "First name must be 2-30 characters long and can contain only letters");
    }
    
    //- Last Name is not required!    
    if (isset($lastName) && !empty($lastName)){
        if (  (strlen($lastName) < 1 || strlen($lastName) > 30 ||
            preg_match("/^[a-zA-Z]+$/", $lastName) === 0 ) ) {
            array_push($errorList, "Last Name must be 1-30 characters long");
        }
    }    
    
    //- Company Name is not required!
    if(isset($companyName) && !empty($companyName)){
       if ( (strlen($companyName) < 2 || strlen($companyName) > 100 ||
             preg_match("/^[a-zA-Z-._0-9]+$/", $companyName) === 0 ) ) {
            array_push($errorList, "Company Name must be 2-100 characters long ");
        }
    }
     
    //- TODO: put some regex on street address
     if (strlen($streetAddress) < 1 || strlen($streetAddress) > 100) {
        array_push($errorList, "Address must be 1-100 characters long");
    }
    
     if (strlen($suiteOrAptNo) < 1 || strlen($suiteOrAptNo) > 10) {
        array_push($errorList, "Suite/apt issue...");
    }

    if (strlen($city) < 1 || strlen($city) > 40) {
        array_push($errorList, "City must be 1-40 characters long");
    }

    //- TODO: ensure valud phone! REgex
      if (strlen($phone) < 1 || strlen($phone) > 100) {
        array_push($errorList, "phone issue...");
    }
    
      if (filter_var($email1, FILTER_VALIDATE_EMAIL) === FALSE) {
        array_push($errorList, "Email is invalid");
        //unset($valueList['email']);
    }
    
    if($email1 != $email2){
        array_push($errorList,"Emails do not match!");    
    }         
    
    //- TODO: regex for pass
       if ($pass1 != $pass2) {
        array_push($errorList, "Passwords do not match");
    } else {
        if (strlen($pass1) < 6 || strlen($streetAddress) > 100) {
            array_push($errorList, "Password must be at lest 6 characters long!");
        }
    }     
  
    if (!$errorList) {
        //----------- State 2: successful submission ---------
        $clientEmail = DB::queryFirstRow("SELECT email FROM clients WHERE email=%s", $email);        
        if($clientEmail){

            $app->render('index.html.twig');
        }
        
        DB::insert('clients', 
                array(
                    'firstName'=>$firstName,
                    'lastName'=>$lastName,
                    'companyName'=>$companyName,
                    'streetAddress'=>$streetAddress,
                    'suiteOrAptNo'=>$suiteOrAptNo,
                    'city'=>$city,
                    'postalCode'=>$postalCode,
                    'phone'=>$phone,
                    'email'=>$email1,
                    'password'=>$pass1                              
                ));
           //- Generating a log message about the current activity
            $log->debug("Adding todo with new id=" . DB::insertId());
            
    } else {
         //------------- State 3: failed submission -----------
   
        $valueList = array('firstName'=>$firstName,
           'lastName'=>$lastName,
            'companyName'=>$companyName,
            'streetAddress'=>$streetAddress,
            'suiteOrAptNo'=>$suiteOrAptNo,
            'city'=>$city,
            'postalCode'=>$postalCode,
            'phone'=>$phone,
            'email1'=>$email1,
            'email2'=>$email2           
            );
        $app->render('register.html.twig', array(
            'v' => $valueList,
            'errorList' => $errorList
                ));
    }
});

$app->get('/isemailregistered/(:email)', function($email = "") use ($app) {
    $user = DB::queryFirstRow("SELECT * FROM clients WHERE email=%s", $email);    
    
    echo ($user) ? "Email already in use" : "";
});
//--- End of dealing with Registration, both get and post


$app->run();